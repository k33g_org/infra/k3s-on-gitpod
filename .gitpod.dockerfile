FROM gitpod/workspace-full

RUN sudo apt update -y && \ 
    sudo apt install -y qemu qemu-system-x86 linux-image-5.4.0-1009-gcp libguestfs-tools sshpass netcat gettext && \
    brew install derailed/k9s/k9s && \
    brew install helm && \
    brew install kubernetes-cli 

USER gitpod

RUN brew install httpie && \
    brew install hey && \
    brew install exa && \
    brew install bat


